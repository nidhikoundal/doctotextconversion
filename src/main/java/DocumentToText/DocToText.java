package DocumentToText;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import ConversionHelper.Conversion;
import ConversionHelper.ErrorParameter;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("/")
public class DocToText {

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to the
	 * client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */

	@GET
	@Path("/Convert")
	public String getStatus() {
		return "<H2> HTTP STATUS : 200 (OK) </H2>";
	}

	@POST
	@Path("/Convert")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public String Convert(@Context HttpServletRequest request, @HeaderParam("Content-Type") String accepted,
		InputStream incomingData) {
		Conversion conversion = new Conversion();
		ErrorParameter error = new ErrorParameter();
		String convertedData = "", response = "";

		String ContentType = request.getContentType();
		String responseType = "";
		if(ContentType.equals("application/xml") && ContentType.equals("application/json")) {
			error.setErrorCode("1026");
			error.setErrorMessage("Invalid Content-Type.");
			return conversion.errorGenerate(error,"XML");
		}else {
			if(ContentType.contains("xml")) {
				responseType = "XML";
			}
			if(ContentType.contains("json")) {
				responseType = "JSON";
			}
		}
		
		
		StringBuilder RchilliBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				RchilliBuilder.append(line);
			}
		} catch (Exception e) {

		}
		String filedata = "";
		String filename = "";
		String userkey = "";
		String version = "";
		String subuserid = "";
		ArrayList<HashMap<String, Boolean>> configParameter = new ArrayList<HashMap<String, Boolean>>();
		try {

			JSONObject jsonObject = new JSONObject(RchilliBuilder.toString());
			
			if(jsonObject.has("version")) {
				version = jsonObject.get("version").toString();
			}else {
				error.setErrorCode("1021");
				error.setErrorMessage("Version parameter is missing.");
				return conversion.errorGenerate(error,responseType);
			}
			
			if(jsonObject.has("fileData")) {
				filedata = jsonObject.get("fileData").toString();
			}else {
				if(version.isEmpty())version = "1.0.1";				
				error.setErrorCode("1022");
				error.setErrorMessage("FileData parameter is missing.");
				return conversion.errorGenerate(error,responseType);
			}
			
			if(jsonObject.has("fileName")) {
				filename = jsonObject.get("fileName").toString();
			}else {
				if(version.isEmpty())version = "1.0.1";
				error.setErrorCode("1023");
				error.setErrorMessage("FileName parameter is missing.");
				return conversion.errorGenerate(error,responseType);
			}
			
			if(jsonObject.has("userKey")) {
				userkey = jsonObject.get("userKey").toString();
			}else {
				if(version.isEmpty())version = "1.0.1";
				error.setErrorCode("1024");
				error.setErrorMessage("UserKey parameter is missing.");
				return conversion.errorGenerate(error,responseType);
			}
			
			if(jsonObject.has("subUserId")) {
				subuserid = jsonObject.get("subUserId").toString();
			}else {
				if(version.isEmpty())version = "1.0.1";
				error.setErrorCode("1025");
				error.setErrorMessage("SubUserId parameter is missing.");
				return conversion.errorGenerate(error,responseType);
			}
			
			
			if(jsonObject.has("configParameter")) {
				JSONArray jsonobject =(JSONArray) jsonObject.get("configParameter");
				
				Iterator objectIterator =  jsonobject.iterator();
			   
			    while(objectIterator.hasNext()) {
			    	HashMap<String, Boolean> configParam = new HashMap<String, Boolean>();
			        JSONObject object = (JSONObject) objectIterator.next();		
			        
			        if(object.has("image")) {
			        	  configParam.put("image", (boolean) object.get("image"));
			        }
			        if(object.has("shape")) {
			        	 configParam.put("shape", (boolean) object.get("shape"));
			        }
			        if(object.has("drawing")) {
			        	configParam.put("drawing", (boolean) object.get("drawing"));
			        }
			        if(object.has("faceImage")) {
			        	configParam.put("faceImage", (boolean) object.get("faceImage"));
			        }
			        if(object.has("asposeHTML")) {
			        	configParam.put("asposeHTML", (boolean) object.get("asposeHTML"));
			        }
			        if(object.has("customHTML")) {
			        	configParam.put("customHTML", (boolean) object.get("customHTML"));
			        }
			        if(object.has("isBold")) {
			        	configParam.put("isBold", (boolean) object.get("isBold"));
			        }
			        configParameter.add(configParam);
			    }

			}
			if (version == null || version.equals("")) {
				error.setErrorCode("1011");
				error.setErrorMessage("Version is required.");
				return conversion.errorGenerate(error,responseType);
			}
			if (filedata == null || filedata.equals("")) {			
				error.setErrorCode("1002");
				error.setErrorMessage("File base64 data is required.");
				return conversion.errorGenerate(error,responseType);
			}
			if (filename == null|| filename.equals("")) {
				error.setErrorCode("1004");
				error.setErrorMessage("File Name is required.");
				return conversion.errorGenerate(error,responseType);
			}

			if (userkey == null || userkey.equals("")) {
				error.setErrorCode("1001");
				error.setErrorMessage("User Key is required.");
				return conversion.errorGenerate(error,responseType);
			}			
			if (subuserid == null || subuserid.equals("")) {
				error.setErrorCode("1002");
				error.setErrorMessage("SubUserId is required.");
				return conversion.errorGenerate(error,responseType);
			}
			if (!version.equals("1.0.1")) {
				error.setErrorCode("1011");
				error.setErrorMessage("Invalid version.");
				return conversion.errorGenerate(error,responseType);
			}
		} catch (Exception e) {
			e.printStackTrace();
			error.setErrorCode("1035");
			error.setErrorMessage("Invalid request. Please check api parameters.");
			return conversion.errorGenerate(error,responseType);
		}
		
		response = conversion.conversion(filedata, filename, "aspose"+version, userkey, subuserid, configParameter, responseType);		
		return response;
	}
}
