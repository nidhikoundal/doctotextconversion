package ConversionHelper;


import java.io.*;



   class Utility
   {
      
      public  static void ErrorLog(Exception exception)
      {  
    	  String ErrorLogFile="errorlog.log";
    	  
    	 try {
    		 Writer writer = new StringWriter();
    		 PrintWriter printWriter = new PrintWriter(writer);
    		 exception.printStackTrace(printWriter);
    		 String message ="Error Message : "+ writer.toString();
    		 writer.close();
    		 printWriter.close();
    		 FileWriter fWriter = null;
    		 BufferedWriter writer1 = null;
          
    		 fWriter = new FileWriter(ErrorLogFile,true);
    		 writer1 = new BufferedWriter(fWriter);        
    		 writer1.write(message);
    		 writer1.newLine();
    		 writer1.close();
    		 fWriter.close();         
          } catch (Exception ex)
          
          {
        	  
          }
    	  
      }
      
      public static boolean StringIsNullOrEmpty(String String)
      {  
   	   
   	   boolean result =false; 
   	   
   	   if(String==null)
   	   {
   		   result=true;    		   
   	   }else if(String.isEmpty())
   	   {
   		   result=true;    		   
   	   }  	   
   	   
   	   return result;
      }
      
      public String Location(String Userkey)
  	{
    	
  		File resume;
  		File dir;
  		String currentdir = System.setProperty("user.dir", GlobalConstant.currentDirectory);
  		if (!System.getProperty("os.name").toUpperCase().contains("WINDOWS"))
  		{
  			resume = new File(Userkey + "/" + "HtmlFiles");
  			dir = new File(currentdir + "/" + resume);
  		} else
  		{
  			resume = new File(Userkey + "\\" + "HtmlFiles");
  			dir = new File(currentdir + "\\" + resume);
  		}

  		String[] myFiles;
  		if (dir.isDirectory())
  		{
  			myFiles = dir.list();
  			for (int i = 0; i < myFiles.length; i++)
  			{
  				File myFile = new File(dir, myFiles[i]);
  				myFile.delete();
  			}
  		}
  		return resume.getAbsolutePath();
  	}
      
      // change 8211 values character to ascii values 255
      static public String removeSpecailCharacter(String ResumeString)
      {
          StringBuilder resumeobj = new StringBuilder();
          resumeobj.append(ResumeString);

          String collrepl [] = Regex.Matches(resumeobj.toString(), "\\d \\? ");

          for (String matchrepl : collrepl)
          {
              Utility.StringBuilderReplace(resumeobj, matchrepl, matchrepl.replace("?", "-"));               
          }

          ResumeString = resumeobj.toString();

          StringBuilder sb = new StringBuilder();
          
          resumeobj.replace(0,resumeobj.length(),"");           
          
          
          for (char ch : ResumeString.toCharArray())
          {               
       	   int a=0;
       	   a=(int)'�';
       	   
       	   if ((int)ch > 13 && (int)ch < 32)
              {
                  sb.append(" ");
              }        	   
       	   else  if ((int)ch ==0)
              {
                  sb.append("");
              }else  if ((int)ch ==1)
              {
                  sb.append("");
              }
              else
              {
                 // #region for specil character change ascii value 8211 to  8243
                             	  
           	   
                  switch ((int)ch)
                  {
                  
                  case 9642:
          		       sb.append("\t");
          		       break;
                 case 9676:
          		       sb.append("\t");
          		       break;
                  case 9679:
          		       sb.append("\t");
          		       break;
                  case 61472:
              		   sb.append(" ");
              		   break;
                  case 61578:
              		   sb.append(" ");
              		   break;
              	   case 61592:
              		   sb.append("\t");
              		   break;
              	   case 61623:
            		   sb.append("\t");
            		   break;	   
              	   case 61480:
           		   sb.append("\t");
           		   break;
              	   case 61498:
              		   sb.append("\t");
              		   break;
              	   case 65533:
           		   sb.append(" ");
           		   break;          		   
              	   case 8205:
              		   sb.append(" ");
        		   break;               	   
              	   
              	   case 8211:                	   
                      sb.append("-");
                      break;
                  case 8212:
                      sb.append("-");
                      break;
                  case 8213:
                      sb.append("-");
                      break;
                  case 8215:
                      sb.append("_");
                      break;
                  
                  case 1418:
                      sb.append("-");
                      break;
                  case 1470:
                      sb.append("-");
                      break;  
                  case 65293:
                      sb.append("-");
                      break;
                  case 65123:
                      sb.append("-");
                      break;
                  case 61599:
                      sb.append("\t");
                      break;
                  case 61557:
                      sb.append("\t");
                      break;
                  case 61607:
               	    sb.append("");
                  case 8209:
                      sb.append("-");
                      break;
                  case 8216:
                      sb.append("'");
                      break;
                  case 8217:
                      sb.append("'");
                      break;
                  case 8218:
                      sb.append("'");
                      break;
                  case 8219:
                      sb.append(",");
                      break;
                  case 8220:
                      sb.append("\"");
                      break;
                  case 8221:
                      sb.append("\"");
                      break;
                  case 8226:
                      sb.append("	");
                      break;
                  case 8364:
                      sb.append(" ");
                      break;
                  case 8340:
                      sb.append("%");
                      break;                       
                  case 8242:
                      sb.append("'");
                      break;
                  case 8243:
                      sb.append("\"");
                      break;
                  case 65306:
               	   sb.append(":");
               	   break;
                  case 160 :
                      sb.append(" ");
                      break;
                  case 167 :
                      sb.append("	");
                      break;
                  case 194 :
                      sb.append("	");
                      break;
                  case 173 :
                      sb.append("-");
                      break;    
                 
                  case 11:
               	   sb.append("\r");
               	   break;
                  case 12:
          		       sb.append("\r");
          		       break;
                  case 7:
               	   sb.append("\t");
               	   break;                  
                  default:
                      sb.append(Character.toString(ch));
                      break;
                  }
                  
              }
          }
          
          return sb.toString();
          //return removeCoverletter(Regex.Replace(sb.toString(), "(( of +)?NUMPAGES|Page[\\s\\d]+of|PAGE[^\\w]+)\\s*\\d*", " ", true,false).replace("�", "-").replace("?", " ")).replace("[", " ").replace("]", " ");
      }
      
      public static  void StringBuilderReplace(StringBuilder StringBuilder,String from, String to)
      {
   	   
   	   if(Utility.StringIsNullOrEmpty(from))
   	   {
   		 return;  
   	   }

   	   if(Utility.StringIsNullOrEmpty(to))
   	   {
   		 return;  
   	   }
   	   
   	   
   	   int index = StringBuilder.indexOf(from);
   	    while (index != -1)
   	    {
   	    	StringBuilder.replace(index, index + from.length(), to);
   	        index += to.length(); // Move to the end of the replacement
   	        index = StringBuilder.indexOf(from, index);
   	    }
      }
      
   
   }

