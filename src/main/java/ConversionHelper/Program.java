package ConversionHelper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class Program {

	public static void main(String[] args) {
		String filePath = "C:\\Users\\rchilli-dev1\\Downloads\\Eva.ioTestREsume.docx";
		if (args.length > 0) {
			filePath = args[0];
		}
		GlobalConstant.htmlPath = "D:\\MyWorkPlace\\Java\\RChilli\\DocumentToTextApi\\src\\RchilliDoc";
		long start = System.currentTimeMillis();
		String base64Data = null;
		InputStream streamData = null;
		ArrayList<HashMap<String, Boolean>> configParameter = new ArrayList<>();
		
		HashMap<String, Boolean> newMap = new HashMap<String, Boolean>();
		newMap.put("isBold",true);
		configParameter.add(newMap);
		try {
			base64Data = Base64.encodeFromFile("C:\\Users\\Nidhi\\Downloads\\SampleResumesForSandbox\\Andrew_Marsh_Resume.DOCX");
			byte[] fileBytedata = Base64.decode(base64Data);
			streamData = new ByteArrayInputStream(fileBytedata);
		} catch (IOException e) {
			e.printStackTrace();
		}

		ConvertdocClass doc = new ConvertdocClass();
		String text = doc.ConvertDocToText(streamData, "fileName.docx", configParameter, "1.0.1", "XML", "");
		System.out.println(text);

		long end = System.currentTimeMillis();
		System.out.println("Time:" + (start - end));

	}
}