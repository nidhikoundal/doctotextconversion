package ConversionHelper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.text.StringEscapeUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.w3c.dom.Element;

import com.aspose.words.Document;
import com.aspose.words.DocumentProperty;

import RchilliConverter.ReadDocFile;
import RchilliConverter.ReadDocxFile;



public class Conversion {

	ErrorParameter error = new ErrorParameter();
	public Conversion() {
		 String Filespliter ="";
		 String APIFolder = Conversion.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();

		 if(!System.getProperty("os.name").toUpperCase().contains("WINDOWS"))
         {
			Filespliter = "/";	
				
         }else
         {
          	Filespliter ="\\";
          	APIFolder=APIFolder.replace("%20"," ");
         }
		   
		 if(APIFolder.toUpperCase().contains("WEB-INF"))
		 {
				
			GlobalConstant.currentDirectory=APIFolder.substring(0,APIFolder.indexOf("WEB-INF"));				
								
			if(Filespliter.contains("\\"))
			{
				GlobalConstant.currentDirectory=APIFolder.substring(1,APIFolder.indexOf("WEB-INF")).replace("/", "\\");
	        }
		}
		   
		GlobalConstant.htmlPath=GlobalConstant.currentDirectory+"HtmlFiles"+Filespliter;
		CreatcheckFolder(GlobalConstant.htmlPath,Filespliter);
		try {
			Context ctx1 = new InitialContext();
			String MaxPageCount =(String) ctx1.lookup("java:comp/env/maxPageCount");
			GlobalConstant.maxPageCount = Integer.parseInt(MaxPageCount);
		} catch (Exception e1) {
			//e1.printStackTrace();
		}
		
	}

	public String conversion(String filedata, String fileName, String version, String userkey, String subuserid,
			ArrayList<HashMap<String, Boolean>> configParameter, String responseType) {
		
		if (version.equals("1.0.1")) {
			error.setErrorCode("1017");
			error.setErrorMessage("Version is not as per agreemnent.");
			return errorGenerate(error,responseType);
		}
		if (version.contains("aspose")) {
			version = version.substring("aspose".length());
		}
		String wordstring = null;

		try {

			byte[] fileBytedata = Base64.decode(filedata);
			InputStream streamData = new ByteArrayInputStream(fileBytedata);
			StringBuilder htmlString = new StringBuilder();

			wordstring = fileProcess(streamData, fileName, htmlString, configParameter, version, responseType, fileBytedata);
			if (wordstring.contains("File size is too large for processing. No. Of pages"))
				return wordstring;

			if (wordstring.equals("") || wordstring.contains("<error>")
					|| wordstring.contains("<TextData><![CDATA[]]></TextData>")) {

				String extension = fileName.substring(fileName.lastIndexOf(".")).toUpperCase();
				if (extension.equals(".DOC")) {

					ReadDocFile Rdoc = new ReadDocFile();
					try {
						wordstring = Rdoc.ReadDocFile(filedata);
					} catch (Exception extype) {
						error.setErrorCode("1017");
						error.setErrorMessage("Not Text Content. Please save this file to proper format and send.");
						return errorGenerate(error,responseType);
					}
				}
				if (extension.equals(".DOCX")) {
					ReadDocxFile Rdocx = new ReadDocxFile();
					try {
						wordstring = Rdocx.ReadDocxFile(filedata);
					} catch (Exception extype) {
						error.setErrorCode("1017");
						error.setErrorMessage("Not Text Content. Please save this file to proper format and send.");
						return errorGenerate(error,responseType);
					}
				}
			}

		} catch (Exception ex) {
			//ex.printStackTrace();
		}

		filedata = null;
		//System.out.println(wordstring);
		return wordstring.replace("&#xd;", " ");

	}
	String getResumeId(InputStream streamData) {
		String HRJ_Identifier = "";
		try {
			Document docOfEnrich = new Document(streamData);
			for (DocumentProperty prop : docOfEnrich.getCustomDocumentProperties()) {
				if (prop.getName().contains("HRJ_IDENTIFIER")) {
					HRJ_Identifier = (String) prop.getValue();
				}
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.getMessage());
		}
		return HRJ_Identifier;
	}
	
	String getMd5Hash(byte[] fileBytedata) {
		String getMd5Hash = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(fileBytedata);

           getMd5Hash = DatatypeConverter.printHexBinary(messageDigest).toUpperCase();
		} catch (Exception e) {
			System.out.println("Error : " + e.getMessage());
		}
		return getMd5Hash;
	}
	
	String fileProcess(InputStream streamData,String fileName, StringBuilder htmlString, ArrayList<HashMap<String, Boolean>> configParameter, String version, String responseType, byte[] fileBytedata)
	 {
		 String wordstring = "" ;
	        htmlString.replace(0, htmlString.length(),"");
	     	try
	        {	 
		            String extension = fileName.substring(fileName.lastIndexOf(".")).toUpperCase();
		            if (extension.equals(".DOC") || extension.equals(".RTF") || extension.equals(".DOCX") 
		            		|| extension.equals(".ODT") || extension.equals(".DOT")|| extension.equals(".DOTX")
		            		|| extension.equals(".DOCM")|| extension.equals(".DOTM"))
		            {	
		            	
		            	ConvertdocClass  convert  = new ConvertdocClass();	
		            	
		            	
		            	String md5Hash = getMd5Hash(fileBytedata);
		            	
		            	wordstring = convert.ConvertDocToText(streamData, fileName, configParameter, version, responseType, md5Hash);
		            	convert=null;
		            	
		            }
		            else
		            {
		            	error.setErrorCode("1017");
						error.setErrorMessage("Resume File Extension not supported. Resume Parser support only doc,docx,dot,rtf,pdf,odt,txt,htm and html files.");
						return errorGenerate(error,responseType);   
			        }
	        }
	        catch (Exception ex)
	        {
	        	error.setErrorCode("1017");
				error.setErrorMessage("Not Text Content. Please save this file to proper format and send.");
				return errorGenerate(error,responseType);		          
	            
	        }
	      return wordstring;
	 }
	
	public String errorGenerate(ErrorParameter errorParameter, String responseType) {
		String xmlString = "";
		try {
			if(responseType.equals("XML")) {
				DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
	 			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
	 			org.w3c.dom.Document doc = docBuilder.newDocument();
	 			
	 			Element error = doc.createElement("error");
	 			error.appendChild(doc.createTextNode(""));
	 			doc.appendChild(error);

	 			Element Code = doc.createElement("code");
	 			Code.appendChild(doc.createTextNode(errorParameter.getErrorCode()));
	 			error.appendChild(Code);
	 			
	 			Element Message = doc.createElement("message");
	 			Message.appendChild(doc.createTextNode(errorParameter.getErrorMessage()));
	 			error.appendChild(Message);
	 			
	 			TransformerFactory transfac = TransformerFactory.newInstance();
	 			Transformer trans = transfac.newTransformer();
	 			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	 			trans.setOutputProperty(OutputKeys.INDENT, "yes");

	 			StringWriter sw = new StringWriter();
	 			StreamResult result = new StreamResult(sw);
	 			DOMSource source = new DOMSource(doc);
	 			trans.transform(source, result);
	 			return xmlString = sw.toString();
			}
			
			
			if(responseType.equals("JSON")) {
				String json = "";
				LinkedHashMap<String, Object> error = new LinkedHashMap<String, Object>();
				LinkedHashMap<String, String> errorMessage = new LinkedHashMap<String, String>();				
				
				errorMessage.put("code", errorParameter.getErrorCode());
				errorMessage.put("message", errorParameter.getErrorMessage());
				error.put("error", errorMessage);				

				json = JSONObject.toJSONString(error);
				json = json.replaceAll("\" *: *null", "\" : \"\"");
				ObjectMapper mapper = new ObjectMapper();
				try {

					Object jsonFormat = mapper.readValue(json, Object.class);

					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonFormat).toString();

				} catch (JsonGenerationException e) {
					Utility.ErrorLog(e);
				} catch (JsonMappingException e) {
					Utility.ErrorLog(e);
				} catch (IOException e) {
					Utility.ErrorLog(e);
				}
				return json;
			}
			
 			
		}catch(Exception ex) {
			//ex.printStackTrace();
		}
		return xmlString;
		
	}
	void CreatcheckFolder(String folderpath,String Filespliter)
	{
		try {
			String Splitpatran = Filespliter;
			if (Filespliter.equals("\\")) {
				Splitpatran += Splitpatran;
			}

			String[] Folders = folderpath.split(Splitpatran);

			StringBuilder Folderpath = new StringBuilder();
			for (String folder : Folders) {
				if (Utility.StringIsNullOrEmpty(folder)) {
					Folderpath.append(Filespliter);
				} else {
					Folderpath.append(folder);
					Folderpath.append(Filespliter);
					File file = new File(Folderpath.toString());
					if (!file.exists()) {
						file.mkdirs();

					}

				}
			}
		} catch (Exception ex) {

		}
	}
	
}
