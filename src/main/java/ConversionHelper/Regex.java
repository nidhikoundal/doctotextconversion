package ConversionHelper;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
class Regex {	
	
	public static Pattern RegexOption; 
	
	
	static String Match(String string,String pattern)	
	{		
		    String result="";
			Pattern matchPatran = Pattern.compile(pattern);
			Matcher matcher = matchPatran.matcher(string);	
			if(matcher.find())
            {
				result= matcher.group();
            }
            return 	result;
	}
	
	static String Match(String string,String pattern,boolean Casesensitive,boolean multiLine ) 
	{	
		 String result="";		 
	    
		 Pattern matchPatran;
		 
		 if(multiLine & Casesensitive)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE|Pattern.CASE_INSENSITIVE);
		 }else if(multiLine)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE);
		 }else if( Casesensitive)			 
		 {
			matchPatran = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
	  	 }		
		 else
	  	 {
	  		matchPatran = Pattern.compile(pattern);	  		 
	  	 }
		
		 Matcher matcher = matchPatran.matcher(string);	
		 if(matcher.find())
         {
				result= matcher.group();
         }
         return result;
	}
	
	static boolean IsMatch(String string,String pattern)
	{	
			Pattern matchPatran = Pattern.compile(pattern);
			Matcher matcher = matchPatran.matcher(string);	
			return matcher.find();
	}
	
	
	static boolean IsMatch(String string,String pattern,boolean Casesensitive,boolean multiLine ) 
	{	
			   
		 Pattern matchPatran;
		 
		 if(multiLine & Casesensitive)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE&Pattern.CASE_INSENSITIVE);
		 }else if(multiLine)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE);
		 }else if( Casesensitive)			 
		 {
			matchPatran = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
	  	 }else
	  	 {
	  		matchPatran = Pattern.compile(pattern);	  		 
	  	 }
		
		 Matcher matcher = matchPatran.matcher(string);	
			return matcher.find();
	}
	
	static String Replace(String string,String pattern, String replacement ) 
	{	
		 String result="";
		 try
		 {
			Pattern matchPatran = Pattern.compile(pattern);
			Matcher matcher = matchPatran.matcher(string);
			result = matcher.replaceAll(replacement);
		 }catch(Exception ex)
		 {			 
			 result = string ;			 
		 }
		 
         return 	result;
	}
	
	static String Replace(String string,String pattern, String replacement,boolean Casesensitive,boolean multiLine ) 
	{	
		
	    //CASE_INSENSITIVE,MULTILINE,CASE_INSENSITIVE,DOTALL,LITERAL,UNICODE_CASE,UNIX_LINES,CANON_EQ 
		 Pattern matchPatran;
		 
		 if(multiLine & Casesensitive)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE&Pattern.CASE_INSENSITIVE);
		 }else if(multiLine)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE);
		 }else if( Casesensitive)			 
		 {
			matchPatran = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
	  	 }else
	  	 {
	  		matchPatran = Pattern.compile(pattern);	  		 
	  	 }	
		 String result="";	
		 try
		 {
			 Matcher matcher = matchPatran.matcher(string);
			 result = matcher.replaceAll(replacement);
		 }catch(Exception ex)
		 {			 
			 result = string ;
		 
		 }
		 return 	result;
	}
	
	static String Removee(String string,String pattern, String replacement,boolean Casesensitive,boolean multiLine ) 
	{	
		
	    //CASE_INSENSITIVE,MULTILINE,CASE_INSENSITIVE,DOTALL,LITERAL,UNICODE_CASE,UNIX_LINES,CANON_EQ 
		 Pattern matchPatran;
		 
		 if(multiLine & Casesensitive)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE&Pattern.CASE_INSENSITIVE);
		 }else if(multiLine)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE);
		 }else if( Casesensitive)			 
		 {
			matchPatran = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
	  	 }else
	  	 {
	  		matchPatran = Pattern.compile(pattern);	  		 
	  	 }
		 String result="";		
	    try
	    {	    		
	    	Matcher matcher = matchPatran.matcher(string);
	    	result = matcher.replaceAll("");
	    }catch(Exception ex)
	    {			 
	    	result = string ;	 
	    }
        return 	result;
	}
	
	static  String [] Matches(String string, String pattern) 
	{		
	    ArrayList<String> result = new ArrayList<String>();
	   
	    try
	    {
	    
		Pattern matchPatran = Pattern.compile(pattern);
		Matcher matcher = matchPatran.matcher(string);
		
		while(matcher.find())
	    {
			result.add(matcher.group());
	    }
	    }catch (Exception ex)
	    {
	    	
	    	
	    }
	    	    
	    Object obj  [] =  result.toArray();
		 
		 String  matchList [] = new String[obj.length]; 
		 
		 for(int i=0; i<obj.length;i++)
        {                 
			 matchList[i]= (String) obj[i];             
        } 
		 
		 return    matchList;
	}	
	
	
	
	
	static String [] Split(String string,String pattern,boolean Casesensitive ,boolean multiLine)
	{
		     
		    //CASE_INSENSITIVE,MULTILINE,CASE_INSENSITIVE,DOTALL,LITERAL,UNICODE_CASE,UNIX_LINES,CANON_EQ 
			 Pattern matchPatran;
			 
			 if(multiLine & Casesensitive)
			 {
				 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE&Pattern.CASE_INSENSITIVE);
			 }else if(multiLine)
			 {
				 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE);
			 }else if( Casesensitive)			 
			 {
				matchPatran = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
		  	 }else
		  	 {
		  		matchPatran = Pattern.compile(pattern);	  		 
		  	 }
			 
			 String [] matchList = matchPatran.split(string);
			 return  matchList;
		
		
	}
	
	
	static String [] Matches(String string,String pattern,boolean Casesensitive ,boolean multiLine)
	{	
		 	
		  ArrayList<String> result = new ArrayList<String>();	 
	    //CASE_INSENSITIVE,MULTILINE,CASE_INSENSITIVE,DOTALL,LITERAL,UNICODE_CASE,UNIX_LINES,CANON_EQ 
		 Pattern matchPatran;
		 
		 if(multiLine & Casesensitive)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE&Pattern.CASE_INSENSITIVE);
		 }else if(multiLine)
		 {
			 matchPatran = Pattern.compile(pattern,Pattern.MULTILINE);
		 }else if( Casesensitive)			 
		 {
			matchPatran = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
	  	 }else
	  	 {
	  		matchPatran = Pattern.compile(pattern);	  		 
	  	 }
		
		 Matcher matcher = matchPatran.matcher(string);	
		 
		 while(matcher.find())         
		 {
				result.add(matcher.group());
		 }

		 Object obj  [] =  result.toArray();
		 
		 String  matchList [] = new String[obj.length]; 
		 
		 for(int i=0; i<obj.length;i++)
         {                 
			 matchList[i]= (String) obj[i];             
         } 
		 
		 return    matchList;		   	    
	}

}
