package ConversionHelper;

import com.aspose.words.IResourceLoadingCallback;
import com.aspose.words.ResourceLoadingAction;
import com.aspose.words.ResourceLoadingArgs;
import com.aspose.words.ResourceType;

class HandleResourceLoadingCallback implements IResourceLoadingCallback {
    public int resourceLoading(ResourceLoadingArgs args) {
    	//System.out.println("Image URL -> "+args.getOriginalUri());
    	
        if (args.getResourceType() == ResourceType.IMAGE) {
            return ResourceLoadingAction.SKIP;
        }
        return ResourceLoadingAction.DEFAULT;
    }
}
