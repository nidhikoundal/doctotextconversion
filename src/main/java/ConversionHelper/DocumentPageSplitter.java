package ConversionHelper;



import com.aspose.words.*;

public class DocumentPageSplitter {
    /// <summary>
    /// Initializes new instance of this class. This method splits the document into sections so that each page
    /// begins and ends at a section boundary. It is recommended not to modify the document afterwards.
    /// </summary>
    /// <param name="collector">A collector instance which has layout model records for the document.</param>
    public DocumentPageSplitter(LayoutCollector collector) throws Exception {
        mPageNumberFinder = new PageNumberFinder(collector);
        mPageNumberFinder.SplitNodesAcrossPages();
    }

    /// <summary>
    /// Gets the document of a page.
    /// </summary>
    /// <param name="pageIndex">1-based index of a page.</param>
    public Document getDocumentOfPage(int pageIndex) throws Exception {
        return getDocumentOfPageRange(pageIndex, pageIndex);
    }

    /// <summary>
    /// Gets the document of a page range.
    /// </summary>
    /// <param name="startIndex">1-based index of the start page.</param>
    /// <param name="endIndex">1-based index of the end page.</param>
    public Document getDocumentOfPageRange(int startIndex, int endIndex) throws Exception {
        Document result = (Document) getDocument().deepClone(false);

        for (Section section : (Iterable<Section>) mPageNumberFinder.RetrieveAllNodesOnPages(startIndex, endIndex, NodeType.SECTION))
            result.appendChild(result.importNode(section, true));

        return result;
    }

    /// <summary>
    /// Gets the document this instance works with.
    /// </summary>
    private Document getDocument() {
        return mPageNumberFinder.getDocument();
    }

    private PageNumberFinder mPageNumberFinder;
}
