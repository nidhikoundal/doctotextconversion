package ConversionHelper;


import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.text.StringEscapeUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GenerateResponse
{
	
	String TextToJSON(String CoverLetter,String doctext,String htmltext, String Image,String ImageName,String pageCount,boolean isBold,String boldResumeId) {
		String json = "";
		LinkedHashMap<String, String> resultData = new LinkedHashMap<String, String>();
		String htmlString = "";
		if(!htmltext.isEmpty()) {
			htmlString = StringEscapeUtils.escapeHtml4(htmltext);
		}
		
		resultData.put("CoverLetter", CoverLetter);
		resultData.put("TextData", doctext);
		resultData.put("HTMLData", htmlString);
		resultData.put("FaceImage", Image);
		resultData.put("FaceImageType", ImageName);
		resultData.put("PageCount", pageCount);
		if(isBold) {
			resultData.put("ResumeId", boldResumeId);
		}
		

		json = JSONObject.toJSONString(resultData);
		//System.out.print(json);
		json = json.replaceAll("\" *: *null", "\" : \"\"");
		ObjectMapper mapper = new ObjectMapper();
		try {

			Object jsonFormat = mapper.readValue(json, Object.class);

			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonFormat).toString();

		} catch (JsonGenerationException e) {
			Utility.ErrorLog(e);
		} catch (JsonMappingException e) {
			Utility.ErrorLog(e);
		} catch (IOException e) {
			Utility.ErrorLog(e);
		}
		return json;
	}
	
	String  TextToXML(String CoverLetter,String doctext,String htmltext, String Image,String ImageName,String pageCount,boolean isBold ,String boldResumeId)
	{
		if(htmltext.contains("writing-mode"))
		{
			htmltext=htmltext.replaceAll("writing-mode:.*?;", "");
		}
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try 
		{
			docBuilder = dbfac.newDocumentBuilder();
		
			Document doc = docBuilder.newDocument();
		
		
		Element rootElementResumeParserData = doc.createElement("DocumentToText");
		doc.appendChild(rootElementResumeParserData);

		
		Element CovLetter = doc.createElement("CoverLetter");
		CovLetter.appendChild(doc.createTextNode("<![CDATA["+ CoverLetter + "]]>"));
		rootElementResumeParserData.appendChild(CovLetter);
		
		
		Element TextData = doc.createElement("TextData");
		TextData.appendChild(doc.createTextNode("<![CDATA["+ doctext + "]]>"));
		rootElementResumeParserData.appendChild(TextData);

		Element elemHTMLData = doc.createElement("HTMLData");
		elemHTMLData.appendChild(doc.createTextNode("<![CDATA["	+ htmltext + "]]>"));
		rootElementResumeParserData.appendChild(elemHTMLData);
		
		Element elemFaceImage = doc.createElement("FaceImage");
		elemFaceImage.appendChild(doc.createTextNode("<![CDATA["+ Image +"]]>"));
		rootElementResumeParserData.appendChild(elemFaceImage);
		
		Element elemFaceImageType = doc.createElement("FaceImageType");
		elemFaceImageType.appendChild(doc.createTextNode("<![CDATA["+ ImageName +"]]>"));
		rootElementResumeParserData.appendChild(elemFaceImageType);
		
		
		Element elemPageCount = doc.createElement("PageCount");
		elemPageCount.appendChild(doc.createTextNode("<![CDATA["+ pageCount +"]]>"));
		rootElementResumeParserData.appendChild(elemPageCount);
		
		if(isBold) {
			Element boldResumeID = doc.createElement("ResumeId");
			boldResumeID.appendChild(doc.createTextNode("<![CDATA["+ boldResumeId +"]]>"));
			rootElementResumeParserData.appendChild(boldResumeID);
		}
		
		// set up a transformer
		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans = transfac.newTransformer();

		trans.setOutputProperty(OutputKeys.METHOD, "xml");
		trans.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		trans.setOutputProperty(OutputKeys.INDENT, "yes");
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		// create string from xml tree
		
		

		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		DOMSource source = new DOMSource(doc);
		System.gc();
		trans.transform(source, result);
		return sw.toString().replace("&lt;", "<").replace("&gt;", ">");
		}
		 catch (Exception ex)
			{
				return "XMLError";
			}
		
	}
	

}
